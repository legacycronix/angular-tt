(function () {
    var cr_ConfirmDialog = function ($scope, $uibModalInstance, parameters) {
        var self = this;
        // ---

        self.modal_title = parameters.title || '';
        self.modal_description = parameters.description || '';
        self.modal_data = parameters.data || '';

        self.message = [];
        self.errors = [];
        self.loading = false;


        $scope.data = {
            "name": "",
            "email": "",
            "password": ""
        };

        $scope.validForm = true;
        $scope.validation = null;

        self.cancel = function () {
            $uibModalInstance.dismiss();
        };


        self.ok = function () {
            $uibModalInstance.close();
        };
        return self;
    };

    // ---

    cr_ConfirmDialog.$inject = [
        '$scope',
        '$uibModalInstance',
        'parameters'
    ];

    angular.module('Cronix-Angular-TT').controller('cr_ConfirmDialog', cr_ConfirmDialog);
})();
