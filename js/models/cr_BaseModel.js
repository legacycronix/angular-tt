(function () {

    /**
     *
     */
    var cr_BaseModel = function () {
        return function (data) {
            var model = {};

            model = angular.extend(model, data || {});
            model.clone = function () {
                return angular.copy(model);
            };

            return model;
        };
    };

    // ---

    cr_BaseModel.$inject = [];

    angular.module('Cronix-Angular-TT').factory('cr_BaseModel', cr_BaseModel);

})();