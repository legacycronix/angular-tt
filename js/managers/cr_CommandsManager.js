angular.module('Cronix-Angular-TT')
    .factory('cr_CommandsManager', ['$rootScope', 'cr_BaseCommand',
        function ($rootScope, cr_BaseCommand) {

            var self = this;

            // ---

            self.cr_BaseCommand = cr_BaseCommand;

            // ---

            return self;
        }]);