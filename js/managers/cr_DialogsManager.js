angular.module('Cronix-Angular-TT')
    .factory('cr_DialogsManager', ['$rootScope', '$uibModal', function ($rootScope, $uibModal) {
        var self = this;

        // ---

        var dialogs = {};

        // ---

        self.type = {
            CONFIRM: 'CONFIRM'
        };

        // ---

        self.registerDialog = function (name, template, controller, size) {
            dialogs[name] = {
                template: template,
                controller: controller,
                size: size
            };
        };

        self.unregisterDialog = function (name) {
            delete dialogs[name];
        };

        self.create = function (name, parameters) {
            var dialog = dialogs[name];

            if (!dialog) {
                return null;
            }

            var modalInstance = $uibModal.open({
                templateUrl: dialog.template,
                controller: dialog.controller + ' as ctrl',
                size: dialog.size || '', // '' 'sm' 'lg'
                resolve: {
                    parameters: function () {
                        return jQuery.extend({
                            title: '',
                            description: ''
                        }, parameters || {});
                    }
                }
            });

            modalInstance.result.then(function () {

            }, function () {

            });

            return modalInstance.result;
        };

        return self;
    }]);