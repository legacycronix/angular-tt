(function () {

    /**
     *
     */
    var cr_RequestService = function ($q, cr_BaseService) {
        var self = this;

        /**
         *
         * @returns {*}
         */
        self.ping = function () {
            return cr_BaseService.get('./pong.json', {});
        };


        return self;
    };

    // ---

    cr_RequestService.$inject = [
        '$q',
        'cr_BaseService'
    ];

    angular.module('Cronix-Angular-TT').service('cr_RequestService', cr_RequestService);

})();