(function () {

    /**
     *
     */
    var cr_NumberUtils = function () {
        var self = this;

        /**
         *
         * @param min
         * @param max
         * @param fractional
         * @returns {*}
         */
        self.random = function (min, max, fractional) {
            return fractional ? Math.random() * (max - min) + min : Math.floor(Math.random() * (max - min + 1)) + min;
        };

        /**
         *
         * @param value
         * @param min
         * @param max
         * @returns {number}
         */
        self.limit = function (value, min, max) {
            return Math.max(min, Math.min(value, max));
        };

        /**
         *
         * @param value
         * @param decimals
         * @returns {number}
         */
        self.round = function (value, decimals) {
            return Number((value).toFixed(decimals));
        };


        return self;
    };

    // ---

    cr_NumberUtils.$inject = [];

    angular.module('Cronix-Angular-TT').factory('cr_NumberUtils', cr_NumberUtils);

})();