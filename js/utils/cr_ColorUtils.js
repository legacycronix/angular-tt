(function () {

    /**
     *
     */
    var cr_ColorUtils = function () {
        var self = this;

        /**
         *
         * @returns {string}
         */
        self.randomColor = function () {
            var letters = '0123456789ABCDEF'.split('');
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        };

        return self;
    };

    // ---

    cr_ColorUtils.$inject = [];

    angular.module('Cronix-Angular-TT').factory('cr_ColorUtils', cr_ColorUtils);

})();