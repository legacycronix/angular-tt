(function () {

    /**
     *
     */
    var cr_LogUtils = function () {
        var self = this;

        /**
         *
         * @param tags
         */
        self.log = function (tags) {
            var args = Array.prototype.slice.call(arguments);

            tags = tags && tags.length ? tags : ['debug'];

            args[0] = tags.join(', ') + ' >';

            console.log.apply(console, args);
        };

        return self;
    };

    // ---

    cr_LogUtils.$inject = [];

    angular.module('Cronix-Angular-TT').factory('cr_LogUtils', cr_LogUtils);

})();