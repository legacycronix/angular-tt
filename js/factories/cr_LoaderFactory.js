angular.module('Cronix-Angular-TT').factory('cr_LoaderFactory', function () {
    return new function Loading() {
        var self = this;

        // ---

        var inUse = false;

        // ---

        self.progress = 0;

        // ---

        self.isLoading = function isLoading() {
            return inUse === true;
        };

        self.load = function load() {

        };

        self.unload = function unload() {

        };
    };
});