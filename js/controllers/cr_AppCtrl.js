(function () {
    var cr_AppCtrl = function ($rootScope, $scope, $element, $timeout, $state, $stateParams, cr_AppFactory, cr_PreloaderFactory, cr_URLManager, cr_DialogsManager) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $rootScope.cr_AppFactory = cr_AppFactory;
        $rootScope.cr_PreloaderFactory = cr_PreloaderFactory;

        // ---

        cr_DialogsManager.registerDialog(cr_DialogsManager.type.CONFIRM, cr_URLManager.dialogs + '/tpl/dialogs/confirm.html', 'cr_ConfirmDialog', 'sm');

        // ---

        cr_PreloaderFactory.progress = 100;

        $timeout(function () {
            cr_PreloaderFactory.hide();
        }, 1000);
    };

    // ---

    cr_AppCtrl.$inject = [
        '$rootScope',
        '$scope',
        '$element',
        '$timeout',
        '$state',
        '$stateParams',
        'cr_AppFactory',
        'cr_PreloaderFactory',
        'cr_URLManager',
        'cr_DialogsManager'
    ];

    angular.module('Cronix-Angular-TT').controller('cr_AppCtrl', cr_AppCtrl);
})();
