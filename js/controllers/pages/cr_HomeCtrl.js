(function () {
    var cr_HomeCtrl = function ($rootScope, $scope, $timeout) {
        var ctrl = this;

        // ---

        var c = jQuery('#c');

        var canvas = new fabric.Canvas(c[0]);

        var imgElement = new Image();
        imgElement.src = '../img/cronix.png';
        imgElement.onload = function () {
            console.log('image load success', this.src);

            var cronix = new fabric.Image(this, {
                opacity: 0.85
            });
            canvas.add(cronix);
        };
        imgElement.onerror = function () {
            console.error('image load error', this.src);
        };

        // ---

        function update() {
            canvas.setWidth(c.parents('.ui-layout-container:eq(0)').width());
            canvas.setHeight(c.parents('.ui-layout-container:eq(0)').height());
            canvas.calcOffset();
        }

        // ---

        function _onDocumentReady() {
            update();
        }

        function _onWindowResize() {
            update();
        }

        // ---

        jQuery(document).on('ready', _onDocumentReady);
        jQuery(window).on('resize', _onWindowResize);

        $scope.$on('ui.layout.resize', function (e, beforeContainer, afterContainer) {
            update();
        });

        // ---

        $timeout(function () {
            update();
        });

        // ---

        return ctrl;
    };

    // ---

    cr_HomeCtrl.$inject = [
        '$rootScope',
        '$scope',
        '$timeout'
    ];

    angular.module('Cronix-Angular-TT').controller('cr_HomeCtrl', cr_HomeCtrl);
})();
