angular.module('Cronix-Angular-TT').config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    
    $urlRouterProvider.otherwise('/app/home');

    $stateProvider
    // App
        .state('app', {
            abstract: true,
            url: '/app',
            templateUrl: 'tpl/app.html'
        })
        .state('app.home', {
            url: '/home',
            templateUrl: 'tpl/pages/home.html',
            controller: 'cr_HomeCtrl as ctrl'
        });
}]);