angular.module('Cronix-Angular-TT')
    .factory('cr_Enums', ['$rootScope', function ($rootScope) {
        var self = this;

        self.GENDER = {
            MALE: 'male',
            FEMALE: 'female',
            UNKNOWN: 'unknown'
        };

        return self;
    }]);