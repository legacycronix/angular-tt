angular.module('Cronix-Angular-TT', [
    // ui
    'ui.bootstrap',
    'ui.router',
    'ui.layout',

    'ngSanitize',

    // other

    'xeditable'
]);