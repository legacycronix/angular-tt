Angular-TT

1) npm install

// =====================================================================================================================

WHAT YOU NEED TO DO:

1) Implement buttons or/and drag & drop functionality for circle, rectangle, triangle and image (on the left sidebar).
When we add image - application ask us to input URL for this image.

2) Redo/Undo manager

We can redo/undo each action: add, remove, rotate, resize, scale and move circle, rectangle, triangle and image objects

You should use "Angular-TT\js\managers\cr_CommandsManager.js" for redo/undo control.

You should use "Angular-TT\js\commands\" for all commands we need.

You should add Redo/Undo buttons and disable/undisable it based on queue we have (if no commands we can't move back (use undo),
if current state is last possible command - we can't move forward (use redo)).


Other ideas and features will be big a plus.

For any questions/ideas contact me:

dmitry@cronix.ms